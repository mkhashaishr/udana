import React,{useEffect} from "react";
import { Stack } from "react-bootstrap";
import Footer from "../Components/Footer";
import Header from "../Components/Header";
import TableData from "../Components/Table";

const TablePage = () => {

    useEffect(() => {
        document.title = `Udana.id | Table Page`
    })

    return (
        <>
            <Stack>
                <Header />
                <TableData />
                <Footer />
            </Stack>
        </>
    )
}

export default TablePage;