import React, {useEffect} from "react";
import { Stack } from "react-bootstrap";
import Header from "../Components/Header";
import Step from "../Components/Step";
import MulaiBisnis from "../Components/MulaiBisnis";
import Contact from "../Components/Contact";
import Footer from "../Components/Footer";


const LandingPage = () => {
    
    useEffect(() => {
        document.title = `Udana.id | Platform Crowdfunding Investasi UMKM`
    })

    return (
        <>
            <Stack>
                <Header />
                <Step />
                <MulaiBisnis />
                <Contact />
                <Footer />
            </Stack>
        </>
    )
}

export default LandingPage;