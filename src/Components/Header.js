import React from "react";
import { Container, Row, Col} from "react-bootstrap";
import Navbar from "./Navbar";

const Header = () => {

    return (
        <div className="header-section" id="header">
            <Navbar />
            <div className="banner-image w-100 d-flex justify-content-center align-items-center" style={{borderBottom: "30px solid #FBCD06"}}>
                <div className="content">
                    <Container>               
                        <Row className="justify-content-start mx-1 mx-xxl-4 px-1 px-xxl-5 position-relative" style={{zIndex: "99"}}>
                            <Col xl="6">
                                <h1 className="ubuntu-text">Jadilah Investor UMKM & Waralaba</h1>
                                <p className="karla-text">
                                    Temukan bisnis UMKM & Waralaba yang anda sukai dan mulai menjadi investor untuk 
                                    kemajuan perekonomian bangsa tanpa perlu pusing dengan operasional sehari-hari
                                </p>
                                <div style={{position: "relative",zIndex: "199"}}>
                                    <button className="btn btn-primary karla-text">
                                        Investasi Sekarang
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-right ps-1" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
                                        </svg>
                                    </button>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </div>
        </div>
    )
}

export default Header;