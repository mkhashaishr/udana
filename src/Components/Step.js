import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import imgBgStep from "../Assets/Img/Step/step-bg.png";
import imgStep1 from "../Assets/Img/Step/step01.png";
import imgStep2 from "../Assets/Img/Step/step02.png";
import imgStep3 from "../Assets/Img/Step/step03.png";

const Step = () => {
    return (
        <div className="step-section mt-5">
            <Container>
                <div className="mx-1 mx-xxl-5 px-1 px-xxl-5 position-relative">
                    <Row className="step-section--header mb-3">
                        <Col lg="5">
                            <h2 className="sectionHeader-text ubuntu-text">Menjadi Investor UMKM itu gampang</h2>
                        </Col>
                        <Col lg="4" xl="3">
                            <p className="sectionDesc-text karla-text pt-lg-3 pt-xl-0">Berinvestasi pada UMKM bisa dilakukan oleh siapa saja dan dimana saja.</p>
                        </Col> 
                    </Row>
                    <Row className="step-section--step justify-content-center">
                        <Col md="6" lg="4" className="mb-5 mb-xl-0 justify-content-center">
                            <img src={imgStep1} alt="Step 1" className="img-fluid" />
                            <div className="position-relative mt-4 px-4">
                                <h3 className="ubuntu-text mb-3 ps-1" style={{fontSize: "25px", fontWeight: "600"}}>Pilih UMKM</h3>
                                <p className="karla-text position-relative ps-1" style={{zIndex: "0",fontSize: "20px"}}>Temukan usaha yang anda minati pada galeri.</p>
                                <span className="ubuntu-text position-absolute pe-3">1</span>
                            </div>
                        </Col>
                        <Col md="6" lg="4" className="mb-5 mb-xl-0">
                            <img src={imgStep2} alt="Step 2" className="img-fluid" />
                            <div className="position-relative mt-4 px-4">
                                <h3 className="ubuntu-text mb-3 ps-1" style={{fontSize: "25px", fontWeight: "600"}}>Analisa Bisnis</h3>
                                <p className="karla-text position-relative ps-1" style={{zIndex: "0",fontSize: "20px"}}>Lakukan analisa profil bisnis dan pengusaha.</p>
                                <span className="ubuntu-text position-absolute pe-3">2</span>
                            </div>
                        </Col>
                        <Col md="6" lg="4" className="mb-5 mb-xl-0">
                            <img src={imgStep3} alt="Step 3" className="img-fluid" />
                            <div className="position-relative mt-4 px-4 position-relative">
                                <h3 className="ubuntu-text mb-3 ps-1" style={{fontSize: "25px", fontWeight: "600"}}>Mulai Berinvestasi</h3>
                                <p className="karla-text position-relative ps-1" style={{zIndex: "0",fontSize: "20px"}}>Menjadi investor startup dengan modal minimal 100.000 Rupiah.</p>
                                <span className="three ubuntu-text position-absolute pe-3">3</span>
                            </div>
                        </Col>
                    </Row>
                    <img src={imgBgStep} alt="Bg Step" className="bgStep" />
                </div>
            </Container>
        </div>
    )
}

export default Step;