import React from "react";
import { Container, Row, Col, Stack} from "react-bootstrap";
import imgMap from "../Assets/Img/Contact/geo-alt.svg";
import imgEnv from "../Assets/Img/Contact/envelope.svg";
import imgPhone from "../Assets/Img/Contact/telephone.svg";
import imgBgContact from "../Assets/Img/Contact/step-bg.png";

const Contact = () => {
    return (
        <div className="contact-section">
            <Container>
                <div className="mx-1 mx-xxl-5 px-1 px-xxl-5 mt-5 position-relative">
                    <div className="bg-primary p-5 position-relative overflow-hidden shadow-sm" style={{borderRadius : "30px"}}>
                        <Row className="align-items-center position-relative" style={{zIndex: "99"}}>
                            <Col lg="5" className="me-4">
                                <h2 className="sectionHeader-text lh-sm text-white ubuntu-text">Kami siap membantu Anda</h2>
                                <p className="text karla-text text-white" style={{width: "95%"}}>Apabila Anda ingin mengetahui lebih lanjut mengenai Udana. Silahkan menghubungi kami</p>
                            </Col>
                            <Col lg="5" className="mt-5 mt-lg-0">
                                <Stack>
                                    <Stack direction="horizontal" gap="3" className="align-items-start">
                                        <img src={imgMap} alt="Map" className="map" />
                                        <Stack className="text-white">
                                            <p className="lh-1 karla-text text-large fw-bold">PT Dana Rintis Indonesia</p>
                                            <p className="address karla-text text-large" style={{marginTop: "-0.5rem"}}>
                                                Jl. KH. Hasyim Ashari No.17, Petojo Utara,
                                                Kecamatan Gambir, Kota Jakarta Pusat
                                                Daerah Khusus Ibukota Jakarta, 10130
                                            </p>
                                        </Stack>
                                    </Stack>
                                    <Stack direction="horizontal" gap="3" className="align-items-start">
                                        <img src={imgEnv} alt="Email" className="email" />
                                        <p className="lh-1 karla-text text-large fw-bold text-white">support@udana.id</p>
                                    </Stack>
                                    <Stack direction="horizontal" gap="3" className="align-items-start">
                                        <img src={imgPhone} alt="Phone" className="PHONE" />
                                        <p className="lh-1 karla-text text-large fw-bold text-white">087709999430</p>
                                    </Stack>
                                </Stack>
                            </Col>
                        </Row>
                        <img src={imgBgContact} alt="Background" className="imgBg" />
                    </div>
                </div>
            </Container>
        </div>
    )
}

export default Contact