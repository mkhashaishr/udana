import React from "react";
import { Stack } from "react-bootstrap";
import FooterLink from "./FooterLink";
import FooterText from "./FooterText";


const Footer = () => {
    return (
        <div className="footer-section">
            <Stack>
                <FooterLink />
                <FooterText />
            </Stack>
        </div>
    )
}

export default Footer;