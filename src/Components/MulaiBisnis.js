import React from "react";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import imgRinduCandu from "../Assets/Img/Bisnis/RinduCandu.png";
import imgMrBig from "../Assets/Img/Bisnis/MrBig.png";
import imgNasiBalap from "../Assets/Img/Bisnis/NasiBalap.png";
import imgNyoklatTeen from "../Assets/Img/Bisnis/NyoklatTeen.png";
import imgHabaHaus from "../Assets/Img/Bisnis/HabaHaus.png";
import imgHelloSugar from "../Assets/Img/Bisnis/HelloSugar.png";
import { NavLink } from "react-router-dom";

const MulaiBisnis = () => {
    return (
        <div className="mulaiBisnis-section">
            <Container className="text-center mb-5">
                <Row>
                    <Col>
                        <h2 className="sectionHeader-text ubuntu-text mb-3">Siap Memulai Berbisnis?</h2>
                        <p className="sectionDesc-text karla-text mb-5">Tentukan bisnis waralaba pilihan Anda sekarang</p>
                    </Col>
                </Row>
                <Row className="mx-1 mx-xxl-5 px-1 px-xxl-5">
                    <div className="w-100 mulaiBisnis-section--line" />
                </Row>
                <Row className="mt-2 mb-4 mx-2 mx-xxl-5 px-2 px-xxl-5">
                    <Col md="6" lg="4" className="p-2">
                        <Card className="text-center shadow-sm card-wrapper">
                            <Card.Header className="border-0 bg-white card-wrapper--header">
                                <h4 className="ubuntu-text">F&B</h4>
                            </Card.Header>
                            <Card.Body className="p-0">
                                <Card.Img src={imgRinduCandu} alt="Rindu Candu" className="rounded-0" style={{height: "200px",objectFit: "cover"}} />
                                <Card.Text className="my-4">
                                    <h3 className="karla-text">Rindu Candu Milk Bar</h3>
                                </Card.Text>
                            </Card.Body>
                            <Button variant="light" className="w-100 py-3 ubuntu-text">LIHAT DETAIL</Button>
                        </Card>
                    </Col>
                    <Col md="6" lg="4" className="p-2">
                        <Card className="text-center shadow-sm card-wrapper">
                            <Card.Header className="border-0 bg-white card-wrapper--header">
                                <h4 className="ubuntu-text">F&B</h4>
                            </Card.Header>
                            <Card.Body className="p-0">
                                <Card.Img src={imgMrBig} alt="Mr Big" className="rounded-0" style={{height: "200px",objectFit: "cover"}} />
                                <Card.Text className="my-4">
                                    <h3 className="karla-text">Mr Big</h3>
                                </Card.Text>
                            </Card.Body>
                            <Button variant="light" className="w-100 py-3 ubuntu-text">LIHAT DETAIL</Button>
                        </Card>
                    </Col>
                    <Col md="6" lg="4" className="p-2">
                        <Card className="text-center shadow-sm card-wrapper">
                            <Card.Header className="border-0 bg-white card-wrapper--header">
                                <h4 className="ubuntu-text">F&B</h4>
                            </Card.Header>
                            <Card.Body className="p-0">
                                <Card.Img src={imgNasiBalap} alt="Nasi Balap" className="rounded-0" style={{height: "200px",objectFit: "cover"}} />
                                <Card.Text className="my-4">
                                    <h3 className="karla-text">Nasi Balap</h3>
                                </Card.Text>
                            </Card.Body>
                            <Button variant="light" className="w-100 py-3 ubuntu-text">LIHAT DETAIL</Button>
                        </Card>
                    </Col>
                    <Col md="6" lg="4" className="p-2">
                        <Card className="text-center shadow-sm card-wrapper">
                            <Card.Header className="border-0 bg-white card-wrapper--header">
                                <h4 className="ubuntu-text">F&B</h4>
                            </Card.Header>
                            <Card.Body className="p-0">
                                <Card.Img src={imgNyoklatTeen} alt="Nyoklat Teen" className="rounded-0" style={{height: "200px",objectFit: "cover"}} />
                                <Card.Text className="my-4">
                                    <h3 className="karla-text">Nyoklat Teen</h3>
                                </Card.Text>
                            </Card.Body>
                            <Button variant="light" className="w-100 py-3 ubuntu-text">LIHAT DETAIL</Button>
                        </Card>
                    </Col>
                    <Col md="6" lg="4" className="p-2">
                        <Card className="text-center shadow-sm card-wrapper">
                            <Card.Header className="border-0 bg-white card-wrapper--header">
                                <h4 className="ubuntu-text">F&B</h4>
                            </Card.Header>
                            <Card.Body className="p-0">
                                <Card.Img src={imgHabaHaus} alt="Haba Haus" className="rounded-0" style={{height: "200px",objectFit: "cover"}} />
                                <Card.Text className="my-4">
                                    <h3 className="karla-text">Haba Haus</h3>
                                </Card.Text>
                            </Card.Body>
                            <Button variant="light" className="w-100 py-3 ubuntu-text">LIHAT DETAIL</Button>
                        </Card>
                    </Col>
                    <Col md="6" lg="4" className="p-2">
                        <Card className="text-center shadow-sm card-wrapper">
                            <Card.Header className="border-0 bg-white card-wrapper--header">
                                <h4 className="ubuntu-text">F&B</h4>
                            </Card.Header>
                            <Card.Body className="p-0">
                                <Card.Img src={imgHelloSugar} alt="Hello Sugar" className="rounded-0" style={{height: "200px",objectFit: "cover"}} />
                                <Card.Text className="my-4">
                                    <h3 className="karla-text">Hello Sugar</h3>
                                </Card.Text>
                            </Card.Body>
                            <Button variant="light" className="w-100 py-3 ubuntu-text">LIHAT DETAIL</Button>
                        </Card>
                    </Col>
                </Row>
                <Row className="justify-content-center">
                    <Col className="col-auto">
                        <NavLink to="/" className="link">
                        <p className="karla-text">Lihat Semua</p>        
                        </NavLink>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default MulaiBisnis;