import React from "react";
import { Container, Row, Col, Stack } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import imgLogo from "../Assets/Img/Logo/udana2.png";
import imgIg from "../Assets/Img/Socmed/instagram.png";
import imgLi from "../Assets/Img/Socmed/linkedin.png";
import imgIso from "../Assets/Img/Supported By/iso.png";
import imgAlu from "../Assets/Img/Supported By/aludi.png";
import imgPse from "../Assets/Img/Supported By/pse.png";
import imgKom from "../Assets/Img/Supported By/kominfo.png";

const FooterLink = () => {
    return(
        <div className="footer-link mt-4" style={{boxShadow: "0 -10px 10px -10px rgba(0, 0, 0, 0.09)"}}>
            <Container>
                <div className="mx-1 mx-xxl-5 px-1 px-xxl-5 mt-2 mb-5">
                    <Row className="p-4">
                        <Col lg="2" className="mb-4 mb-lg-0 p-0">
                            <img src={imgLogo} alt="Udana Logo" className="logo img-fluid py-2" />
                            <p className="karla-text text-center" style={{fontWeight: "700", fontSize: "12px"}}>PT. Dana Rintis Indonesia</p>
                        </Col>
                        <Col lg="2" className="mb-4 mb-lg-0">
                            <ul className="address karla-text">
                                <li>
                                    Jl. KH. Hasyim Ashari No.17, Petojo Utara,
                                    Kecamatan Gambir, Kota Jakarta Pusat, Daerah Khusus
                                    Ibukota Jakarta, 10130
                                </li>
                                <li>
                                    E: <b>support@udana.id</b>
                                </li>
                                <li>
                                    W: <b>087709999430</b>
                                </li>
                            </ul>
                        </Col>
                        <Col lg="2" className="col-6">
                            <p className="header karla-text fw-bold lh-1">Perusahaan</p>
                            <ul className="link karla-text" style={{margin: "0", padding: "0", listStyleType: "none", fontSize: "14px", lineHeight: "2"}}>
                                <li>
                                    <NavLink to="/">
                                        Tentang
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink to="/">
                                        Hubungi Kami
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink to="/">
                                        Syarat & Ketentuan
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink to="/">
                                        Kebijakan Privasi
                                    </NavLink>
                                </li>
                            </ul>
                        </Col>
                        <Col lg="2" className="col-6 align-self-end align-self-lg-start">
                            <p className="header karla-text fw-bold lh-1">Investor</p>
                            <ul className="link karla-text" style={{margin: "0", padding: "0", listStyleType: "none", fontSize: "14px", lineHeight: "2"}}>
                                <li>
                                    <NavLink to="/">
                                        FAQ
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink to="/">
                                        Resiko Investasi
                                    </NavLink>
                                </li>
                            </ul>
                        </Col>
                        <Col lg="2" className="my-4 my-lg-0 align-self">
                            <p className="header karla-text fw-bold lh-1">Social Media</p>
                            <div className="d-inline-flex">
                                <img src={imgIg} alt="Instagram" style={{width: "25px"}} />
                                <img src={imgLi} alt="LinkedIn" style={{width: "25px"}} />
                            </div>
                        </Col>
                        <Col lg="2">
                            <p className="header karla-text fw-bold lh-1">Didukung oleh</p>
                            <Stack className="pe-3">
                                <Row className="p-0 g-0">
                                    <Col className="col-4">
                                        <img src={imgKom} alt="Kominfo" className="img-fluid" />
                                    </Col>
                                    <Col className="col-4">
                                        <img src={imgPse} alt="PSE" className="img-fluid" />
                                    </Col>
                                    <Col className="col-4">
                                        <img src={imgAlu} alt="Aludi" className="img-fluid" />
                                    </Col>
                                </Row>
                                <img src={imgIso} alt="KAN" />
                            </Stack>
                        </Col>
                    </Row>
                </div>
            </Container>
        </div>
    )
    
}

export default FooterLink;