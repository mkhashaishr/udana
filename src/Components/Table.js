import React, {useState, useEffect} from "react";
import { Container, Row, Col, Table } from "react-bootstrap";
import axios from "axios";


const TableData = () => {

    const [data, setData] = useState([])
    const [fetchStatus, setFetchStatus] = useState(true)

    useEffect(() => {

        const fetchData = async () => {
            const res = await axios.get(`https://api.openbrewerydb.org/breweries`);
            let resData = res.data;

            setData(
                resData.map((e) => {

                    let {
                        id,
                        name,
                        city,
                        postal_code,
                        state,
                        country
                    } = e

                    return {
                        id,
                        name,
                        city,
                        postal_code,
                        state,
                        country
                    }

                }) 
            )
        }

        if(fetchStatus){
            fetchData()
            setFetchStatus(false)
        }
    }, [fetchStatus, setFetchStatus])

    return (
        <div className="table-section">
            <Container>
                <Row className="mb-3">
                    <Col>
                        <h2 className="ubuntu-text text-center">
                            Table Data
                        </h2>
                    </Col>
                </Row>
                <Row className="mx-1 mx-xxl-5 px-1 px-xxl-5">
                    <Col>
                        <Table striped bordered hover responsive>
                            <thead>
                                <tr className="ubuntu-text">
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Postal Code</th>
                                    <th>Country</th>
                                </tr>
                            </thead>
                            <tbody className="karla-text">
                                {data !== null && (
                                    <>
                                        {
                                            data.map((e, index) => {
                                                return (                                                    
                                                    <tr key={index}>
                                                        <td>{index + 1}</td>
                                                        <td>{e.name}</td>
                                                        <td>{e.city}</td>
                                                        <td>{e.postal_code}</td>
                                                        <td>{e.state}</td>
                                                        <td>{e.country}</td>
                                                    </tr>                                                    
                                                )
                                            })
                                        }
                                    </>
                                )}
                            </tbody> 
                        </Table>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default TableData;