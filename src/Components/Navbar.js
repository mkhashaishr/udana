import React, {useState} from "react";
import { NavLink } from "react-router-dom";
import imgLogo1 from "../Assets/Img/Logo/udana1.png";
import imgLogo2 from "../Assets/Img/Logo/udana-logo-black.png";

const Navbar = () => {

    const [navbar, setNavbar] = useState(false);

    const changeBackround = () => {
        if (window.scrollY >= 80) {
            setNavbar(true)
        } else {
            setNavbar(false)
        }
    }

    window.addEventListener('scroll', changeBackround);

    return (
        <nav className={navbar ?  `navbar active fixed-top navbar-expand-lg navbar-light justify-content-center d-flex` : `navbar fixed-top navbar-expand-lg navbar-dark justify-content-center d-flex`}>
            <div className="container">
                <NavLink className="navbar-brand" to="/">
                    { navbar ? 
                        <img src={imgLogo2} alt="Web Logo" className="logo" />:
                        <img src={imgLogo1} alt="Web Logo" className="logo" />                                                               
                    }
                </NavLink>
                <button
                className="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation"
                >
                <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <NavLink to="/" className="nav-link karla-text">Waralaba</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/" className="nav-link karla-text">FAQ</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/" className="nav-link karla-text">Tentang</NavLink>
                    </li>
                </ul>
                <div className="mx-auto"></div>
                <ul className="navbar-nav d-flex align-items-center gap-lg-3">
                    <li className="nav-item">
                        <a href="/#header" className="nav-link karla-text">Masuk</a>
                    </li>
                    <li className="nav-item">
                        <button className="btn btn-primary karla-text py-2 px-3" style={{fontSize: "14px", borderRadius: "8px"}}>Mulai daftar</button>
                    </li>
                </ul>
                </div>
            </div>
        </nav>
    )
}

export default Navbar;