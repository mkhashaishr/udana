import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import LandingPage from "../Pages/LandingPage";
import TablePage from "../Pages/TablePage";

const Routes = () => {

    return (
        <>
            <Router>
                <Switch>
                    <Route exact component={LandingPage} path="/" />
                    <Route exact component={TablePage} path="/table" />
                </Switch>
            </Router>
        </>
    )
}

export default Routes;