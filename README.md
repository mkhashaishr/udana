![Udana](https://i.ibb.co/NCfgPKk/udana.png)

## Udana.id Front End Developer Assessment Tests

Repo : [Link](https://gitlab.com/mkhashaishr/udana/).

Demo : [Link](https://myudana.netlify.app/).

Pertanyaan/instruksi:

1. Remake web https://udana.id/ halaman depan saja (menggunakan framework angular/react js).
2. Design UI/tampilan sesuai dengan web yang sudah ada
3. Sertakan link Sorce Code di git dan link website yang sudah dideploy (nilai + jika deploy) lalu kirimkan balasan pada email ini
4. Jika belum selesai tidak masalah untuk dikumpulkan

Fitur-fitur:

1. Web wajib responsive
2. Tambahan, bikin 1 halaman dengan table yang menampilkan data2 dari API ini [Link](https://api.openbrewerydb.org/breweries) dengan kolom Name, City, State, Postal Code, dan Country.
